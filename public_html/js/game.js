var myGame = (function (game) {
    var entities = [];

    return{
        addEntity: function (entity) {
            entities.push(entity);
        },
        preload: function (game) {
            entities.forEach(function (entity) {
                entity.preload(game);
            });
        },
        create: function (game) {
            entities.forEach(function (entity) {
                entity.create(game);
            });
        },
        update: function (game) {
            entities.forEach(function (entity) {
                entity.update(game);
            });
        },
        restart: function (game) {
            entities.forEach(function (entity) {
                entity.restart(game);
            });
        }
    };
})();
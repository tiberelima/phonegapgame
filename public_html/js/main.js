
var score = 0;
var explosions;

var phaserGame = new Phaser.Game('100%', '100%', Phaser.CANVAS, '', {
    preload: function () {
        myGame.addEntity(background);
        myGame.addEntity(player);
        myGame.addEntity(life);
        myGame.addEntity(enemy);
        myGame.addEntity(emitter);

        myGame.preload(phaserGame);

        phaserGame.load.audio('blast', 'assets/audio/fx/Blast.mp3');
        phaserGame.load.audio('explosion', 'assets/audio/fx/Explosion_Ultra_Bass.mp3');

        phaserGame.load.spritesheet('kaboom', 'assets/explosao.png', 84, 71.6);
    },
    create: function () {
        phaserGame.physics.startSystem(Phaser.Physics.ARCADE);
        myGame.create(phaserGame);

        blast = phaserGame.add.audio('blast');
        explosion = phaserGame.add.audio('explosion');

        scoreText = phaserGame.add.text(12, 12, 'Score: ' + score, {fontSize: '16px', fill: '#f5f5f5'});

        stateText = phaserGame.add.text(phaserGame.world.centerX, phaserGame.world.centerY, ' ', {font: '84px Arial', fill: '#fff'});
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        explosions = phaserGame.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(setupInvader, enemies);
    },
    update: function () {
        myGame.update(phaserGame);
        phaserGame.physics.arcade.overlap(bullets, enemies, this.bulletCollisionHandler, null, this);
        phaserGame.physics.arcade.overlap(player, enemies, this.playerCollisionHandler, null, this);

        scoreText.text = 'score: ' + score;
    },
    bulletCollisionHandler: function (bullet, enemy) {
        score += 10;
        blast.play();
        bullet.kill();
        enemy.kill();
        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(enemy.body.x, enemy.body.y);
        explosion.play('kaboom', 30, false, true);
    },
    playerCollisionHandler: function (player, enemy) {
        enemy.kill();

        live = lives.getFirstAlive();
        if (live) {
            live.kill();

            blast.play();

            if (lives.countLiving() === 3) {
                player.animations.play('low_damage');
            } else if (lives.countLiving() === 2) {
                player.animations.play('medium_damage');
            } else if (lives.countLiving() === 1) {
                player.animations.play('high_damage');
            }
        }
        if (lives.countLiving() < 1) {
            player.kill();
            explosion.play();
            emitter.on = false;

            stateText.text = " GAME OVER\nClick para reiniciar";
            stateText.visible = true;

            phaserGame.input.onTap.addOnce(this.restart, this);
        }
    },
    restart: function () {
        myGame.restart(phaserGame);
        stateText.visible = false;
    }
});

function setupInvader(enemy) {
    console.log(enemy.name);
    enemy.anchor.x = 0;
    enemy.anchor.y = 0;
    enemy.animations.add('kaboom');

}
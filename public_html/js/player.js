var player = (function (game) {
    var bulletTime = 0;
    var playerSpeed = 150;

    return{
        preload: function (game) {
            game.load.spritesheet('bullet', 'assets/tiro.png', 51, 51);
            game.load.spritesheet('nave', 'assets/nave.png', 100, 80);

            game.load.audio('laser', 'assets/audio/fx/laser.mp3');
        },
        create: function (game) {

            cursors = game.input.keyboard.createCursorKeys();
            player = game.add.sprite(232, game.world.height - 350, 'nave');
            game.physics.arcade.enable(player);
            player.body.collideWorldBounds = true;
            player.animations.add('low_damage', [2, 5, 8, 11, 14], 10, true);
            player.animations.add('medium_damage', [1, 4, 7, 10, 13], 10, true);
            player.animations.add('high_damage', [0, 3, 7, 9], 10, true);
            player.frame = 2;

            bullets = game.add.group();
            bullets.enableBody = true;
            bullets.physicsBodyType = Phaser.Physics.ARCADE;
            for (var i = 0; i < 20; i++) {
                var b = bullets.create(0, 0, 'bullet');
                b.name = 'bullet' + i;
                b.exists = false;
                b.visible = false;
                b.checkWorldBounds = true;
                b.events.onOutOfBounds.add(this.resetBullet, this);
            }

            bulletSound = game.add.audio('laser');
            emitter.bindTo(player);
        },
        update: function (game) {
            game.physics.arcade.moveToPointer(player, playerSpeed);
            if (game.input.activePointer.isDown) {
                this.fireBullet(game);
            }
        },
        restart: function (game) {
            player.revive();

            player.animations.stop(null, true);
            player.frame = 2;
            player.x = 32;
            player.y = game.world.height - 150;
        },
        fireBullet: function (game) {

            if (game.time.now > bulletTime && player.alive) {
                bulletSound.play();

                bullet = bullets.getFirstExists(false);
                bullet.animations.add('fire', [0, 1, 2, 3, 4, 5], 10, true);
                bullet.animations.play('fire');
                if (bullet) {
                    bullet.reset(player.x + 90, player.y + 12);
                    bullet.body.velocity.x = 300;
                    bulletTime = game.time.now + 150;
                }
            }
        },
        resetBullet: function (bullet) {
            bullet.kill();
        },
        collisionHandler: function (bullet, veg) {
            bullet.kill();
        }
    };
})();

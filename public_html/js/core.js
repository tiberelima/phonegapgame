requirejs.config({
    //baseUrl: '../',
    /*paths: {
        js: 'js',
        lib: '../lib/'
    }*/
});

requirejs(
    [
        'player',
        'emitter',
        'background',
        'life',
        'enemy',
        'game',
        'main'
    ]
);

var enemy = (function (game) {
    var numOfEnemies = 30;
    var activeEnemies = [];
    return{
        preload: function (game) {
            game.load.spritesheet('enemy', 'assets/inimigo1.png', 96, 96);
            game.load.image('enemy2', 'assets/inimigo2_1.png', 80, 80);
        },
        create: function (game) {
            /*enemies = [];
             for (var i = 0; i < numOfEnemies; i++) {
             enemies.push(new Enemy());
             }*/
            enemies = game.add.group();
            enemies.enableBody = true;
            enemies.physicsBodyType = Phaser.Physics.ARCADE;

            for (var i = 0; i < numOfEnemies; i++) {
                var enemy = enemies.create(game.width + game.world.randomX, game.world.randomY, 'enemy');
                enemy.name = 'enemy' + i;
                enemy.exists = true;
                enemy.visible = true;
                scale = game.rnd.realInRange(.5, .5);
                enemy.scale.x = scale;
                enemy.scale.y = scale;
                activeEnemies.push(enemy);
            }
            for (var i = 0; i < numOfEnemies; i=i+2) {
                var enemy2 = enemies.create(game.width + game.world.randomX, game.world.randomY, 'enemy2');
                enemy2.name = 'enemy2' + i;
                enemy2.exists = true;
                enemy2.visible = true;
                scale = game.rnd.realInRange(.7, .5);
                enemy2.scale.x = scale;
                enemy2.scale.y = scale;
                activeEnemies.push(enemy2);
            }
        },
        update: function (game) {
            /*for (var i = 0; i < numOfEnemies; i++) {
             game.physics.collide(player, enemies[i].enemySprite, killPlayer, null, this)
             }*/
            activeEnemies.forEach(function (enemy) {
                enemy.body.velocity.x = -125;
            });
        },
        restart: function (game) {
            enemies.removeAll();
            enemy.create(phaserGame);
        }
    };
})();

/*Enemy = function () {
 this.x = game.world.randomX;
 this.y = game.world.randomY;
 this.minSpeed = -75;
 this.maxSpeed = 75;
 this.vx = Math.random() * (this.maxSpeed - this.minSpeed + 1) - this.minSpeed;
 this.vy = Math.random() * (this.maxSpeed - this.minSpeed + 1) - this.minSpeed;
 
 this.enemySprite = game.add.sprite(this.x, this.y, "enemy");
 this.enemySprite.anchor.setTo(0.5, 0.5);
 this.enemySprite.body.collideWorldBounds = true;
 this.enemySprite.body.bounce.setTo(1, 1);
 this.enemySprite.body.velocity.x = this.vx;
 this.enemySprite.body.velocity.y = this.vy;
 this.enemySprite.body.immovable = true;
 }*/
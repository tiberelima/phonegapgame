var background = (function (game) {

    return{
        preload: function (game) {
            game.load.image('space', 'assets/space.jpg');
            game.load.audio('theme', 'assets/audio/fx/Army_Strong_Theme_Song.mp3');
        },
        create: function (game) {
            background = game.add.tileSprite(0, 0, 800, 600, 'space');
            theme = game.add.audio('theme');
            theme.play();
            //space = game.background.autoScroll(-20, 0);
        },
        update: function (game) {
            background.tilePosition.x -= 1;
        },
        restart: function (game) {
            theme.restart();
        }
    };
})();
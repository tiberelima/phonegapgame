var emitter = (function (game) {
    var entityEmitter;

    return{
        bindTo: function (entity) {
            entityEmitter = entity;
        },
        preload: function (game) {
            game.load.image('fire', 'assets/fire1.png');
        },
        create: function (game) {
            emitter = game.add.emitter(game.world.centerX, game.world.centerY, 0);
            emitter.makeParticles(['fire']);

            emitter.setRotation(0, 0);
            emitter.setAlpha(0.3, 0.1);
            emitter.gravity = 0;
            emitter.setXSpeed(0, -750);
            emitter.setYSpeed(0, 0);

            emitter.start(false, 500, 10);
        },
        update: function (game) {
            emitter.emitX = entityEmitter.x - 60;
            emitter.emitY = entityEmitter.y + 30;
        },
        restart: function (game) {
            emitter.on = true;
        }
    };
})();
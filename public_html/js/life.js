var life = (function (game) {
    var totalLives = 4;
    return {
        preload: function (game) {
            game.load.image('life', 'assets/heart.png');
        },
        create: function (game) {
            lives = game.add.group();

            for (var i = 0; i < totalLives; i++) {
                var life = lives.create(game.width - 42 - (32 * i), 12, 'life');
                life.name = 'life' + i;
                life.exists = true;
                life.visible = true;
                scale = game.rnd.realInRange(.1, .1);
                life.scale.x = scale;
                life.scale.y = scale;
            }
        },
        update: function (game) {
        },
        restart: function (game) {
            lives.callAll('revive');
        }
    };
})();